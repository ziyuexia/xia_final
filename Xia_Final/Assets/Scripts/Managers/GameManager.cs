﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int m_NumRoundsToWin = 3;        
    public float m_StartDelay = 3f;         
    public float m_EndDelay = 3f;           
    public CameraControl m_CameraControl;   
    public Text m_MessageText;              
    public GameObject m_PlayerPrefab;         
    public TankManager[] m_Players;

    public GameObject storyCanvas;

    public bool Shell;
    public bool Gun;
    public bool Flash;
    public bool CD;

    float flashCD = 10f;
    float coolDownCD = 30f;
    float shieldCD = 60f;

    public Text p1w;
    public Text p2w;
    public Text p1s1;
    public Text p1s2;
    public Text p2s1;
    public Text p2s2;

    private int m_RoundNumber;              
    private WaitForSeconds m_StartWait;     
    private WaitForSeconds m_EndWait;       
    private TankManager m_RoundWinner;
    private TankManager m_GameWinner;

    private bool finishedShop;





    private void Start()
    {
        StartGame();
    }

    public void StartGame()
    {
        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);

        


        StartCoroutine(GameLoop());
    }


    private void SpawnAllTanks()
    {
        for (int i = 0; i < m_Players.Length; i++)
        {
            m_Players[i].m_Instance =
                Instantiate(m_PlayerPrefab, m_Players[i].m_SpawnPoint.position, m_Players[i].m_SpawnPoint.rotation) as GameObject;
            m_Players[i].m_PlayerNumber = i + 1;
            m_Players[i].Setup();
        }
    }

    private void DestroyAllTanks()
    {
        for (int i = 0; i < m_Players.Length; i++)
        {
            if(m_Players[i].m_Instance != null)
            GameObject.Destroy(m_Players[i].m_Instance);
        }
    }
    

    private void SetCameraTargets()
    {
        Transform[] targets = new Transform[m_Players.Length];

        for (int i = 0; i < targets.Length; i++)
        {
            targets[i] = m_Players[i].m_Instance.transform;
        }

        m_CameraControl.m_Targets = targets;
    }


    private IEnumerator GameLoop()
    {
        print("game loop");
        DestroyAllTanks();
        yield return StartCoroutine(RoundShop());
        SpawnAllTanks();
        SetCameraTargets();
        yield return StartCoroutine(RoundStarting());
        yield return StartCoroutine(RoundPlaying());
        yield return StartCoroutine(RoundEnding());

        if (m_GameWinner != null)
        {
            SceneManager.LoadScene(0);
            print("scene load");
        }
        else
        {
            StartCoroutine(GameLoop());
            print("gameloop");
        }
    }


    private IEnumerator RoundShop()
    {
        print("round shop");
        OpenShop();
        while (!finishedShop)
        {
            yield return null;
        }
        yield return null;
    }

    private IEnumerator RoundStarting()
    {
        ResetAllTanks();
        DisableTankControl();

        m_CameraControl.SetStartPositionAndSize();

        m_RoundNumber++;
        m_MessageText.text = "ROUND " + m_RoundNumber;
        print("round+");

        yield return m_StartWait;
    }


    private IEnumerator RoundPlaying()
    {
        EnableTankControl();

        m_MessageText.text = string.Empty;

        while (!OneTankLeft())
        {
            yield return null;
        }
    }


    private IEnumerator RoundEnding()
    {
        DisableTankControl();

        m_RoundWinner = null;
        m_RoundWinner = GetRoundWinner();

        if(m_RoundWinner != null)
        {
            m_RoundWinner.m_Wins++;
            print("win increment" + m_RoundWinner.m_Wins);
        }

        m_GameWinner = GetGameWinner();

        string message = EndMessage();
        m_MessageText.text = message;

        yield return m_EndWait;
    }


    private bool OneTankLeft()
    {
        int numTanksLeft = 0;

        for (int i = 0; i < m_Players.Length; i++)
        {
            if (m_Players[i].m_Instance.activeSelf)
                numTanksLeft++;
        }

        return numTanksLeft <= 1;
    }


    private TankManager GetRoundWinner()
    {
        for (int i = 0; i < m_Players.Length; i++)
        {
            if (m_Players[i].m_Instance.activeSelf)
                return m_Players[i];
        }

        return null;
    }


    private TankManager GetGameWinner()
    {
        for (int i = 0; i < m_Players.Length; i++)
        {
            if (m_Players[i].m_Wins == m_NumRoundsToWin)
            {
                print("player wins" + m_Players[i].m_Wins + "round to win" + m_NumRoundsToWin);
  
                return m_Players[i];
            }
               
        }

        return null;
    }


    private string EndMessage()
    {
        string message = "DRAW!";

        if (m_RoundWinner != null)
            message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";

        message += "\n\n\n\n";

        for (int i = 0; i < m_Players.Length; i++)
        {
            message += m_Players[i].m_ColoredPlayerText + ": " + m_Players[i].m_Wins + " WINS\n";
        }

        if (m_GameWinner != null)
            message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";

        return message;
    }


    private void ResetAllTanks()
    {
        for (int i = 0; i < m_Players.Length; i++)
        {
            m_Players[i].Reset();
        }
    }


    private void EnableTankControl()
    {
        for (int i = 0; i < m_Players.Length; i++)
        {
            m_Players[i].EnableControl();
        }
    }


    private void DisableTankControl()
    {
        for (int i = 0; i < m_Players.Length; i++)
        {
            m_Players[i].DisableControl();
        }
    }

    public void FinishShop()
    {
        Time.timeScale = 1f;
        storyCanvas.SetActive(false);
        finishedShop = true;
    }

    private void OpenShop()
    {
        print("open shop");
        Time.timeScale = 0f;
        storyCanvas.SetActive(true);
        finishedShop = false;
    }

    public void SetWeapon(int choice)
    {
        if(choice == 0)
        {
            m_Players[0].shell = true;
            m_Players[0].gun = false;
            p1w.text = "Weapon: Shell";
        }

        else if(choice == 1)
        {

            m_Players[0].gun = true;
            m_Players[0].shell = false;
            p1w.text = "Weapon: Gun";
        }

        if (choice == 3)
        {
            m_Players[1].shell = true;
            m_Players[1].gun = false;
            p2w.text = "Weapon: Shell";
        }

        else if (choice == 4)
        {

            m_Players[1].gun = true;
            m_Players[1].shell = false;
            p2w.text = "Weapon: Gun";
        }
    }

    public void SetP1Skill(int choice)
    {
        if (choice == 0)
        {
            m_Players[0].skill1 = 1;
            m_Players[0].skill1CD = flashCD;
            p1s1.text = "Spell 1: Flash";
        }

        else if (choice == 1)
        {
            m_Players[0].skill1 = 2;
            m_Players[0].skill1CD = coolDownCD;
            p1s1.text = "Spell 1: Reset CD";
        }

        else if (choice == 2)
        {
            m_Players[0].skill1 = 3;
            m_Players[0].skill1CD = shieldCD;
            p1s1.text = "Spell 1: Shield";
        }

        else if (choice == 3)
        {
            m_Players[0].skill2 = 1;
            m_Players[0].skill2CD = flashCD;
            p1s2.text = "Spell 2: Flash";
        }

        else if (choice == 4)
        {
            m_Players[0].skill2 = 2;
            m_Players[0].skill2CD = coolDownCD;
            p1s2.text = "Spell 2: Reset CD";
        }

        else if (choice == 5)
        {
            m_Players[0].skill2 = 3;
            m_Players[0].skill2CD = shieldCD;
            p1s2.text = "Spell 2: Shield";
        }
    }

    public void SetP2Skill(int choice)
    {
        if (choice == 0)
        {
            m_Players[1].skill1 = 1;
            m_Players[1].skill1CD = flashCD;
            p2s1.text = "Spell 1: Flash";
        }

        else if (choice == 1)
        {
            m_Players[1].skill1 = 2;
            m_Players[1].skill1CD = coolDownCD;
            p2s1.text = "Spell 1: Reset CD";
        }

        else if (choice == 2)
        {
            m_Players[1].skill1 = 3;
            m_Players[1].skill1CD = shieldCD;
            p2s1.text = "Spell 1: Shield";
        }

        else if (choice == 3)
        {
            m_Players[1].skill2 = 1;
            m_Players[1].skill2CD = flashCD;
            p2s2.text = "Spell 2: Flash";
        }

        else if (choice == 4)
        {
            m_Players[1].skill2 = 2;
            m_Players[1].skill2CD = coolDownCD;
            p2s2.text = "Spell 2: Reset CD";
        }

        else if (choice == 5)
        {
            m_Players[1].skill2 = 3;
            m_Players[1].skill2CD = shieldCD;
            p2s2.text = "Spell 2: Shield";
        }
    }
}