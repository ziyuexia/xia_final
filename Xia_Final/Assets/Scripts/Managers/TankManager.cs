﻿using System;
using UnityEngine;

[Serializable]
public class TankManager
{
    public Color m_PlayerColor;            
    public Transform m_SpawnPoint;
    [HideInInspector] public int m_PlayerNumber;
    [HideInInspector] public string m_ColoredPlayerText;
    [HideInInspector] public GameObject m_Instance;
    [HideInInspector] public int m_Wins;                     


    private CharacterMovement m_Movement;       
    private CharacterShooting m_Shooting;
    private TankHealth m_TankHealth;
    private GameObject m_CanvasGameObject;

    public bool shell = false;
    public bool gun = false;

    public int skill1;
    public int skill2;
    public float skill1CD;
    public float skill2CD;



    public void Setup()
    {
        m_Movement = m_Instance.GetComponent<CharacterMovement>();
        m_Shooting = m_Instance.GetComponent<CharacterShooting>();
        m_TankHealth = m_Instance.GetComponent<TankHealth>();
        m_CanvasGameObject = m_Instance.GetComponentInChildren<Canvas>().gameObject;

        m_Movement.m_PlayerNumber = m_PlayerNumber;
        m_Shooting.m_PlayerNumber = m_PlayerNumber;
        m_TankHealth.m_PlayerNumber = m_PlayerNumber;
        Debug.Log ("set up");
        m_TankHealth.SetColor();

        m_Shooting.shell = shell;
        m_Shooting.gun = gun;

        m_Shooting.skill1 = skill1;
        m_Shooting.skill2 = skill2;
        m_Shooting.skill1CD = skill1CD;
        m_Shooting.skill2CD = skill2CD;

        Debug.Log("Player: " + skill1);
        Debug.Log("Player: " + skill2);





        m_ColoredPlayerText = "<color=#" + ColorUtility.ToHtmlStringRGB(m_PlayerColor) + ">PLAYER " + m_PlayerNumber + "</color>";

        MeshRenderer[] renderers = m_Instance.GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material.color = m_PlayerColor;
        }
    }


    public void DisableControl()
    {
        m_Movement.enabled = false;
        m_Shooting.enabled = false;

        m_CanvasGameObject.SetActive(false);
    }


    public void EnableControl()
    {
        m_Movement.enabled = true;
        m_Shooting.enabled = true;

        m_CanvasGameObject.SetActive(true);
    }


    public void Reset()
    {
        m_Instance.transform.position = m_SpawnPoint.position;
        m_Instance.transform.rotation = m_SpawnPoint.rotation;

        m_Instance.SetActive(false);
        m_Instance.SetActive(true);
    }
}
