﻿using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public int m_PlayerNumber = 1;
    public float m_Speed = 5f;
    public float m_TurnSpeed = 180f;

    private string m_MovementVAxisName;
    private string m_MovementHAxisName;
    private Rigidbody m_Rigidbody;
    private float m_MovementVInputValue;
    private float m_MovementHInputValue;
    Vector3 movement;
    //private float m_OriginalPitch;         


    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
    }


    private void OnEnable()
    {
        m_Rigidbody.isKinematic = false;
        m_MovementVInputValue = 0f;
        m_MovementHInputValue = 0f;
    }


    private void OnDisable()
    {
        m_Rigidbody.isKinematic = true;
    }


    private void Start()
    {
        m_MovementVAxisName = "Vertical" + m_PlayerNumber;
        m_MovementHAxisName = "Horizontal" + m_PlayerNumber;
    }


    private void Update()
    {
        // Store the player's input and make sure the audio for the engine is playing.
        m_MovementVInputValue = Input.GetAxis(m_MovementVAxisName);
        m_MovementHInputValue = Input.GetAxis(m_MovementHAxisName);

    }


    /*private void EngineAudio()
    {
        // Play the correct audio clip based on whether or not the tank is moving and what audio is currently playing.
        if(Mathf.Abs(m_MovementInputValue) < 0.1f && Mathf.Abs(m_TurnInputValue) < 0.1f)
        {
            if(m_MovementAudio.clip == m_EngineDriving)
            {
                m_MovementAudio.clip = m_EngineIdling;
                m_MovementAudio.pitch = Random.Range(m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
                m_MovementAudio.Play();
            }
        }
        else
        {
            if (m_MovementAudio.clip == m_EngineIdling)
            {
                m_MovementAudio.clip = m_EngineDriving;
                m_MovementAudio.pitch = Random.Range(m_OriginalPitch - m_PitchRange, m_OriginalPitch + m_PitchRange);
                m_MovementAudio.Play();
            }
        }
    }
    */

    private void FixedUpdate()
    {
        // Move and turn the tank.
        Move(m_MovementHInputValue, m_MovementVInputValue);
    }


    private void Move(float h, float v)
    {
        movement.Set(h, 0f, v);

        movement = movement.normalized * m_Speed * Time.deltaTime;

        m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
    }
}