﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterShooting : MonoBehaviour
{
    public int m_PlayerNumber;
    public Rigidbody m_Shell;
    public Transform m_FireTransform;
    public Slider m_AimSlider;
    public AudioSource m_ShootingAudio;  
    public AudioClip m_ChargingClip;     
    public AudioClip m_FireClip;
    public float m_MinLaunchForce = 15f;
    public float m_MaxLaunchForce = 30f;
    public float m_MaxChargeTime = 0.75f;

    public Transform gunTipTransform;

    public MeshRenderer shieldMesh;
    public Material p1;
    public Material p2;
    public float shieldHealth = 100f;
    public GameObject shield;
    public float shieldTimer;
    public float shieldCurrentHealth;
    public bool shielded;
    public LayerMask shieldMask;


    private string m_FireButton;
    private string m_Skill1Button;
    private string m_Skill2Button;
    private float m_CurrentLaunchForce;
    private float m_ChargeSpeed;
    private bool m_Fired;
    private bool flashed;

    float gunCD = 0.15f;
    float shellCD = 1f;
    float weaponTimer;
    float skill1Timer;
    float skill2Timer;


    public Text P1Skill1Text;
    public Text P1Skill2Text;
    public Text P2Skill1Text;
    public Text P2Skill2Text;

    private string skill1T;
    private string skill2T;

    public bool shell;
    public bool gun;

    public int skill1;
    public int skill2;
    public float skill1CD;
    public float skill2CD;

    float flashCD = 10f;
    float coolDownCD = 30f;
    float shieldCD = 60f;

    public Transform gunTransform;

    Rigidbody m_Rigidbody;
    Vector3 movement;

    public float range = 1f;


    Ray shootRay = new Ray();
    RaycastHit shootHit;
    public LayerMask shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;
    TankHealth tH;

    public AudioSource effect;
    public AudioClip flash;
    public AudioClip shieldEffect;


    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        gunParticles = GetComponentInChildren<ParticleSystem>();
        gunLine = GetComponentInChildren<LineRenderer>();
        gunAudio = GetComponentInChildren<AudioSource>();
        gunLight = GetComponentInChildren<Light>();
        tH = GetComponent<TankHealth>();
    }

    private void OnEnable()
    {
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_AimSlider.value = m_MinLaunchForce;
    }


    private void Start()
    {
        m_FireButton = "Fire" + m_PlayerNumber;
        m_Skill1Button = "Skill1-" + m_PlayerNumber;
        m_Skill2Button = "Skill2-" + m_PlayerNumber;

        shieldTimer = 0f;

        // print(skill1);
        //print(skill2);
        SetS1T(skill1);
        SetS2T(skill2);

        flashed = false;
        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;

        weaponTimer = shellCD;
        skill1Timer = skill1CD;
        skill2Timer = skill2CD;
        print("S1CD " + skill1CD);
        print(skill2CD);

        //print("m_ChargeSpeed:" + m_ChargeSpeed);
        print(shootableMask);

        GunColor();
        ShieldColor();

        
    }

    private void Update()
    {
        weaponTimer += Time.deltaTime;
        skill1Timer += Time.deltaTime;
        skill2Timer += Time.deltaTime;

        //print(m_Skill1);
        //print(shielded);

        CDText();

        CastSkill1(skill1);
        CastSkill2(skill2);


        if(!flashed)
        {
            if (shell)
            {
                ShellFire();
                //print("shell:" + weaponTimer);
            }
            else if (gun)
            {
                MachineGun();
            }
        }
    }

    private void FixedUpdate()
    {
            Flash();
    }

    private void CDText()
    {
        if (m_PlayerNumber == 1)
        {
            int timer1 = Mathf.FloorToInt(skill1CD - skill1Timer);
            int timer2 = Mathf.FloorToInt(skill2CD - skill2Timer);

            if (timer1 >= 0)
            {
                P1Skill1Text.text = "P1 U: " + timer1;
            }
            else
            {
                P1Skill1Text.text = "P1 U: " + skill1T;
            }

            if (timer2 >= 0)
            {
                P1Skill2Text.text = "P1 O: " + timer2;
            }
            else
            {
                P1Skill2Text.text = "P1 O: " + skill2T;
            }
        }

        if (m_PlayerNumber == 2)
        {
            int timer1 = Mathf.FloorToInt(skill1CD - skill1Timer);
            int timer2 = Mathf.FloorToInt(skill2CD - skill2Timer);
            if (timer1 >= 0)
            {
                P2Skill1Text.text = "P2 [7]: " + timer1;
            }
            else
            {
                P2Skill1Text.text = "P2 [7]: " + skill1T;
            }

            if (timer2 >= 0)
            {
                P2Skill2Text.text = "P2 [9]: " + timer2;
            }
            else
            {
                P2Skill2Text.text = "P2 [9]: " + skill2T;
            }
        }
    }

    private void ShellFire()
    {
        // Track the current state of the fire button and make decisions based on the current launch force.
        m_AimSlider.value = m_MinLaunchForce;

        Shoot();

        if (weaponTimer >= shellCD)
        {
            if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)
            {
                //  print("A" + m_CurrentLaunchForce);
                m_CurrentLaunchForce = m_MaxLaunchForce;
                Fire();
            }
            else if (Input.GetButtonDown(m_FireButton))
            {
                m_Fired = false;
                m_CurrentLaunchForce = m_MinLaunchForce;

                //print(Input.GetKey(KeyCode.I));
                m_ShootingAudio.clip = m_ChargingClip;
                m_ShootingAudio.Play();
            }
            else if (Input.GetButton(m_FireButton) && !m_Fired)
            {

                //   print("C1" + m_CurrentLaunchForce);
                m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;

                //print("C" + m_CurrentLaunchForce);
                m_AimSlider.value = m_CurrentLaunchForce;
            }
            else if (Input.GetButtonUp(m_FireButton) && !m_Fired)
            {
                Fire();
                //  print("B");
            }
        }


    }


    private void Fire()
    {
        // Instantiate and launch the shell.
        m_Fired = true;
        weaponTimer = 0f;

        Rigidbody shellInstance = Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

        shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;

        m_ShootingAudio.clip = m_FireClip;
        m_ShootingAudio.Play();

        m_CurrentLaunchForce = m_MinLaunchForce;
    }

    private void Shoot()
    {
        if (m_PlayerNumber == 1)
        {
            //print("Shell1");
            if (Input.GetKey(KeyCode.I))
            {
                gunTransform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
                //print("I");
            }
            else if (Input.GetKey(KeyCode.J))
            {
                gunTransform.rotation = Quaternion.Euler(new Vector3(0f, -90f, 0f));
            }
            else if (Input.GetKey(KeyCode.K))
            {
                gunTransform.rotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
            }
            else if (Input.GetKey(KeyCode.L))
            {
                gunTransform.rotation = Quaternion.Euler(new Vector3(0f, 90f, 0f));
            }
        }

        if (m_PlayerNumber == 2)
        {
            //print("fire2");
            if (Input.GetKey(KeyCode.Keypad8))
            {
                gunTransform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
            }
            else if (Input.GetKey(KeyCode.Keypad4))
            {
                gunTransform.rotation = Quaternion.Euler(new Vector3(0f, -90f, 0f));
            }
            else if (Input.GetKey(KeyCode.Keypad5))
            {
                gunTransform.rotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
            }
            else if (Input.GetKey(KeyCode.Keypad6))
            {
                gunTransform.rotation = Quaternion.Euler(new Vector3(0f, 90f, 0f));
            }
        }
    }

    private void Flash()
    {
        if(flashed)
        {
            effect.clip = flash;
            if (m_PlayerNumber == 1)
            {
                if (Input.GetKey(KeyCode.I))
                {
                    movement.Set(0f, 0f, 4f);
                    m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
                    flashed = false;
                    effect.Play();
                }
                else if (Input.GetKey(KeyCode.J))
                {
                    movement.Set(-4f, 0f, 0f);
                    m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
                    flashed = false;
                    effect.Play();
                }
                else if (Input.GetKey(KeyCode.K))
                {
                    movement.Set(0f, 0f, -4f);
                    m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
                    flashed = false;
                    effect.Play();
                }
                else if (Input.GetKey(KeyCode.L))
                {
                    movement.Set(4f, 0f, 0f);
                    m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
                    flashed = false;
                    effect.Play();
                }

                

            }

            if (m_PlayerNumber == 2)
            {
                if (Input.GetKey(KeyCode.Keypad8))
                {
                    movement.Set(0f, 0f, 4f);
                    m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
                    flashed = false;
                    effect.Play();
                }
                else if (Input.GetKey(KeyCode.Keypad4))
                {
                    movement.Set(-4f, 0f, 0f);
                    m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
                    flashed = false;
                    effect.Play();
                }
                else if (Input.GetKey(KeyCode.Keypad5))
                {
                    movement.Set(0f, 0f, -4f);
                    m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
                    flashed = false;
                    effect.Play();
                }
                else if (Input.GetKey(KeyCode.Keypad6))
                {
                    movement.Set(4f, 0f, 0f);
                    m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
                    flashed = false;
                    effect.Play();
                }
            }
        }
    }

    public void DisableEffects()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    private void MachineGun()
    {
        if (Input.GetButton(m_FireButton) && weaponTimer >= gunCD && !flashed)
        {
            MGShoot();
        }

        if (weaponTimer >= gunCD * effectsDisplayTime)
        {
                DisableEffects();
        }
    }

    private void MGShoot()
    {
        Shoot();
        weaponTimer = 0f;

        gunAudio.Play();

        gunLight.enabled = true;

        gunParticles.Stop();
        gunParticles.Play();

        gunLine.enabled = true;
        gunLine.SetPosition(0, gunTipTransform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
        {

            TankHealth tankHealth = shootHit.collider.GetComponent<TankHealth>();
            CharacterShooting cS = shootHit.collider.GetComponent<CharacterShooting>();
            if (tankHealth != null)
            {
                if (cS.shielded)
                {
                    DamageShield(1f);
                }
                else
                {
                    tankHealth.TakeDamage(1);
                    print("gun not shielded: " + shielded);
                }
            }
            gunLine.SetPosition(1, shootHit.point);
        }

        else
        {
            gunLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
        }
    }



    private void GunColor()
    {
        if(m_PlayerNumber == 1)
        {
            // A simple 2 color gradient with a fixed alpha of 1.0f.
            float alpha = 1.0f;
            Gradient gradient = new Gradient();
            gradient.SetKeys(
                new GradientColorKey[] { new GradientColorKey(Color.red, 0.0f), new GradientColorKey(Color.white, 1.0f) },
                new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
                );
            gunLine.colorGradient = gradient;
        }

        if (m_PlayerNumber == 2)
        {
            // A simple 2 color gradient with a fixed alpha of 1.0f.
            float alpha = 1.0f;
            Gradient gradient = new Gradient();
            gradient.SetKeys(
                new GradientColorKey[] { new GradientColorKey(Color.blue, 0.0f), new GradientColorKey(Color.white, 1.0f) },
                new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
                );
            gunLine.colorGradient = gradient;
        }

    }

    private void ShieldColor()
    {
        if(m_PlayerNumber == 1)
        {
            shieldMesh.material = p1;
        }

        if (m_PlayerNumber == 2)
        {
            shieldMesh.material = p2;
        }
    }

    private void CastShield()
    {
        effect.clip = shieldEffect;
        effect.Play();
        shield.SetActive(true);
        shieldCurrentHealth = shieldHealth;
        shielded = true;
        StopCoroutine(ShieldTimer());
        StartCoroutine(ShieldTimer());
        print("shielded" + shielded);
    }

    public void DamageShield(float amount)
    {
        if (shieldCurrentHealth > amount)
        {
            shieldCurrentHealth -= amount;
        }
        else if (shieldCurrentHealth == amount)
        {
            TurnOffShield();
        }
        else if (shieldCurrentHealth < amount)
        {
            float characterDamage = amount - shieldCurrentHealth;
            TurnOffShield();
            tH.TakeDamage(characterDamage);  
        }

        print(shieldCurrentHealth);
    }

    public void TurnOffShield()
    {
        StopCoroutine(ShieldTimer());
        shield.SetActive(false);
        shielded = false;
        print("not shielded" + shielded);
    }

    private IEnumerator ShieldTimer()
    {
        yield return new WaitForSeconds(5f);
        TurnOffShield();
    }

    private void CastSkill1(int key)
    {
        if (key == 1)
        {
            if (Input.GetButton(m_Skill1Button) && skill1Timer >= flashCD)
            {
                flashed = true;
                skill1Timer = 0f;
                //print("reset skillCD");
            }

        }

        else if (key == 2)
        {
            if (Input.GetButton(m_Skill1Button) && skill1Timer >= coolDownCD)
            {
                skill1Timer = 0f;
                skill2Timer = skill2CD;
                print("reset skill");
            }
        }

        else if (key == 3)
        {
            if (Input.GetButton(m_Skill1Button) && skill1Timer >= shieldCD)
            {
                CastShield();
                skill1Timer = 0f;
            }
        }
    }

    private void CastSkill2(int key)
    {
        if (key == 1)
        {
            if (Input.GetButton(m_Skill2Button) && skill2Timer >= flashCD)
            {
                flashed = true;
                skill2Timer = 0f;
            }

        }

        else if (key == 2)
        {
            if (Input.GetButton(m_Skill2Button) && skill2Timer >= coolDownCD)
            {
                skill1Timer = skill1CD;
                skill2Timer = 0f;
                print("reset skill");
            }
        }

        else if (key == 3)
        {
            if (Input.GetButton(m_Skill2Button) && skill2Timer >= shieldCD)
            {
                CastShield();
                skill2Timer = 0f;
            }
        }
    }

    private void SetS1T(int key)
    {
        if(key == 1)
        {
            skill1T = "Flash";
        }
        else if (key == 2)
        {
            skill1T = "Reset CD";
        }
        else if (key == 3)
        {
            skill1T = "Shield";
        }
    }

    private void SetS2T(int key)
    {
        if (key == 1)
        {
            skill2T = "Flash";
        }
        else if (key == 2)
        {
            skill2T = "Reset CD";
        }
        else if (key == 3)
        {
            skill2T = "Shield";
        }
    }
}