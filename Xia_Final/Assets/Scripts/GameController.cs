﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public GameObject storyCanvas;
    public GameManager gM;

	// Use this for initialization
	void Awake ()
    {
        Time.timeScale = 0f;
	}
	
	// Update is called once per frame
	void Update ()
    {

	}

    public void StartGame()
    {
        Time.timeScale = 1f;
        storyCanvas.SetActive(false);

        gM.StartGame();
    }
}
